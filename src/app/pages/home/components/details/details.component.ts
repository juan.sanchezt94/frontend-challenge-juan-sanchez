import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Holiday } from '../../model/home.model';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  countryCode: any = this.route.snapshot.paramMap.get('code');
  holidays : Holiday[] = [];

  constructor(
    private apiHome : HomeService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    const data = {
      "country_code": this.countryCode,
      "year": 2022
    }
    this.apiHome.postCountryHolidays(data).subscribe(
      (data: any) => {
        console.log(data);
        this.holidays = data.holidays;
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
