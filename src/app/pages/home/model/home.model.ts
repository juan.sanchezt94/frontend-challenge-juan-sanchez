export interface Country {
    code: string;
    name: string;
}

export interface Holiday {
    date:         Date;
    name:         string;
    local_name:   string;
    country_code: string;
    regions:      string[];
    types:        string[];
}