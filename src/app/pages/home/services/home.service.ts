import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Country } from '../model/home.model';
import { HttpClient } from '@angular/common/http';

const httpHeaders = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer ' + environment.apiKey
}

@Injectable({
  providedIn: 'root'
})

export class HomeService {

  constructor(
    private http: HttpClient,
  ) { }

  getCountries() {
    return this.http.get(`https://api.m3o.com/v1/holidays/Countries`, { headers: httpHeaders })
  }

  postCountryHolidays(data : any) {
    return this.http.post(`https://api.m3o.com/v1/holidays/List`, data, { headers: httpHeaders })
  }
}
